//console.log("Hello");

// MINI ACTIVITY
/*
	1. Using the ES6 update, get the cube of 8.
	2. print the result on the console with the message: "The cube of 8 is" + the result
	3. use the template literal in printing the message.
*/

const num = 8 ** 3;
console.log(`The cube of 8 is ${num}.`);




// MINI ACTIVITY
/*
1.Destructure the address array
2. Print the values in the consolse: I live at 258 Washington Avenue, California, 99011
3. use the template literals
4. send the output on hangouts.
*/

const address = ["258", 'Washington Ave NW', 'California', '90011']

const [houseNum, street, state, zip] = address;
console.log(`I live at ${houseNum} ${street}, ${state}, ${zip}`);




//MINI ACTIVITY
const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20ft 3in'
}

const { name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`)




//MINI ACTIVITY
/*
	1. loop through the number using forEach
	2. print the number in the console
	3. Use the .reduce operatpr on the numbers array
	4. assign the result on a variable
	5. print the variable on the console
*/
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((numbers) =>{
	console.log(`${numbers}`);
})
let reducedNumber = numbers.reduce((x, y) => x + y);
console.log(reducedNumber)




//MINI ACTIVITY
/*
	1. create a "dog" class
	2. inside of the class dog, have a name, age and breed
	3. instantiate a new dog class and print in the console.
	4. Send the ss of the output on hangouts.
*/

class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let newDog = new dog('Max', 5, "Husky");
console.log(newDog);
